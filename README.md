# Introduction du service
Test technique d'entrée dans la société Adot
- L'API expose des information sur le events, elle a donc été appelée event.
- L'endpoint demandé retourne la segmentation et le compte d'events pour un point d'intérêt spécifié. 
- Afin de respecter au mieux la norme REST, la méthode POST a été utilisée dans ce cas précis.

# Scripts

## démarrage
npm start

## test
npm run test

## Documentation
### POST /event/count/byPointOfInterest
- Description: expose le nombre et la ventilation des events classés par leur proximité à des points d'intérêt donnnés.
- Header: Content-Type: application/json; charset=utf-8
- Paramêtre: json body, ie: 
```
{
	"points_of_interest": [{
        "lat": 48.86,
        "lon": 2.35,
        "name": "Chatelet"
      }, {
        "lat": 48.8759992,
        "lon": 2.3481253,
        "name": "Arc de triomphe"
    }]
}
```
- Réponse: 
```
{
    "response": {
        "Chatelet": {
            "lat": 48.86,
            "lon": 2.35,
            "name": "Chatelet",
            "impressions": 139086,
            "clicks": 16678
        },
        "Arc de triomphe": {
            "lat": 48.8759992,
            "lon": 2.3481253,
            "name": "Arc de triomphe",
            "impressions": 60914,
            "clicks": 7316
        }
    }
}
```