import express from 'express'
const routes = require('./routes/routes')

// Set up the express app
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.json())

app.use('/', routes)

app.listen(5000, () => {
  console.log(`server running on port ${5000}`)
});
