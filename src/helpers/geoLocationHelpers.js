import geolib from 'geolib'

export const getDistance = (pinA, pinB, accuracy) => 
  geolib.getDistanceSimple(
    pinA,
    pinB,
    accuracy
  )

export const findNearest = (pin, collection, offset = 0) => {
  return geolib.findNearest(pin, collection, offset)
}
