import csvtojson from 'csvtojson'
import fs from 'fs'
import csv from 'fast-csv'

export const loadCSVFileAsJson = (filename) => 
  csvtojson().fromFile(filename)

export const loadCSVFileAsArray = (filename) => {
  return new Promise(function(resolve, reject){
    let result = []
    fs.createReadStream(filename)
    .pipe(csv())
    .on('data',function(data) {
      result.push(data)
    })
    .on('end', function(data){
        result.splice(0,1)
        resolve(result)
    })
  })
}