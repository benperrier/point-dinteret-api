export const formatResponse = ((request, result) => {
  let response = {}
  request.forEach((itm, index) => {
    response[itm.name] = {
      ...itm,
      impressions : result[index].imp,
      clicks: result[index].click
    }
  })
  return response
})