import { getDistance, findNearest } from '../geoLocationHelpers'

const londonEye = {latitude: 51.503333, longitude: -0.119722}
const eiffelTower = {latitude: 48.8583, longitude: 2.2945}
var spots = {
  "Brandenburg Gate, Berlin": {lat: 52.516272, lon: 13.377722},
  "Dortmund U-Tower": {lat: 51.515, lon: 7.453619},
  "London Eye": {lat: 51.503333, lon: -0.119722},
  "Kremlin, Moscow": {lat: 55.751667, lon: 37.617778},
  "Eiffel Tower, Paris": {lat: 48.8583, lon: 2.2945},
  "Riksdag building, Stockholm": {lat: 59.3275, lon: 18.0675},
  "Royal Palace, Oslo": {lat: 59.916911, lon: 10.727567}
}

const spots_array = [
  {lat: 52.516272, lon: 13.377722},
  {lat: 51.515, lon: 7.453619},
  {lat: 51.503333, lon: -0.119722}]

describe('findNearest json', async () => {
  it('should return the key of the exact location (distance:0) searched for', () => {
    expect(findNearest(spots["Brandenburg Gate, Berlin"], spots_array)).toEqual({"distance": 0, "key": "0"})
    expect(findNearest(spots["Dortmund U-Tower"], spots_array)).toEqual({"distance": 0, "key": "1"})
    expect(findNearest(spots["London Eye"], spots_array)).toEqual({"distance": 0, "key": "2"})
  })
})

describe('findNearest array', async () => {
  it('should return the key of the exact location (distance:0) searched for', () => {
    expect(findNearest([spots_array[0].lon, spots_array[0].lat], spots_array)).toEqual({"distance": 0, "key": "0"})
    expect(findNearest([spots_array[1].lon, spots_array[1].lat], spots_array)).toEqual({"distance": 0, "key": "1"})
    expect(findNearest([spots_array[2].lon, spots_array[2].lat], spots_array)).toEqual({"distance": 0, "key": "2"})
  })
})


describe('getDistance', () => {
  it('should calculate the distance between London and Paris (341km)', () =>{
    expect(getDistance(londonEye, eiffelTower, 100)).toEqual(341000)
  })
})

