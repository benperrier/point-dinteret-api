import { loadCSVFileAsArray, loadCSVFileAsJson } from '../loadEvents'

const filename = './resources/events.csv'

describe('loadCSVFileAsArray json', async () => {
  it('should return a json file', () => {
    console.log(process.cwd())
    console.log(loadCSVFileAsArray(filename))
  })
})

describe('loadCSVFileAsJson array', async () => {
  it('should return an array', () => {
    expect(loadCSVFileAsJson(filename)).resolves.toBe('peanut butter')
  })
})
