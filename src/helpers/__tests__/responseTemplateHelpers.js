import { formatResponse } from '../responseTemplateHelper'

describe('findNearest json', async () => {
  it('should return the key of the exact location (distance:0) searched for', () => {
    const output = {
      "Chatelet": {
          "lat": 48.86,
          "lon": 2.35,
          "name": "Chatelet",
          "impressions": 136407,
          "clicks": 16350
      },
      "Arc de triomphe": {
          "lat": 48.8759992,
          "lon": 2.3481253,
          "name": "Arc de triomphe",
          "impressions": 63593,
          "clicks": 7646
      } 
    }
    const request = [{
        "lat": 48.86,
        "lon": 2.35,
        "name": "Chatelet"
      }, {
        "lat": 48.8759992,
        "lon": 2.3481253,
        "name": "Arc de triomphe"
    }]
    const result = [{
        "imp": 136407,
        "click": 16350
      },{
        "imp": 63593,
        "click": 7646
    }]
    expect(formatResponse(request, result)).toEqual(output)
  })
})