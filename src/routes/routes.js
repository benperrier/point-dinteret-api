const routes = require('express').Router();
import { loadCSVFileAsJson } from '../helpers/loadEvents'
import { findNearest } from '../helpers/geoLocationHelpers'
import { formatResponse } from '../helpers/responseTemplateHelper'

const EVENTS_PATH = './resources/events.csv'

loadCSVFileAsJson(EVENTS_PATH).then(events => {
  
  routes.post('/api/v1/event/count/byPointOfInterest', (req, res) => {
    let request = req.body.points_of_interest
    let result = request.map(itm => ({ imp:0, click:0 }))
    events.forEach(item => {
      const nearestIndex = findNearest(item, request).key
      result[nearestIndex][item.event_type]++
    })
    const response = formatResponse(request, result)
    res.status(200).json({ response })
  })

})

module.exports = routes